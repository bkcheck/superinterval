const MAX_INTERVAL = Math.pow(2, 31) - 1
const superinterval = require('./src/superinterval')(MAX_INTERVAL)

export default superinterval
