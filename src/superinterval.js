const assert = require('assert')

function getSuperInterval (maxInterval) {
  /**
   * SuperInterval
   * @param {function} handler The function to execute
   * @param {number} interval The interval in milliseconds
   * @param {object} args Arguments to be passed to the handler function
   */
  class SuperInterval {
    constructor (handler, interval, args) {
      assert.equal(typeof handler, 'function', 'handler is not a valid function.')
      assert.equal(typeof interval, 'number', 'interval is not a valid number')
      assert(interval > 0, 'interval must be greater than 0')

      let self = this

      self.interval = Math.round(interval)
      self.args = args || {}
      self.remainingMs = 0

      self.execute = function () {
        // If interval can be handled by setInterval, just fire normally
        if (self.interval <= maxInterval) {
          handler(self.args)
          return
        }

        // If execute is ready to be fired and rescheduled with the max interval
        if (self.remainingMs <= 0) {
          handler(self.args)
          self.remainingMs = self.interval - maxInterval
          self.token = setTimeout(self.execute, maxInterval)
          return
        }

        // If there is more time remaining before fire than max interval, reschedule at max interval
        if (self.remainingMs > maxInterval) {
          self.remainingMs -= maxInterval
          self.token = setTimeout(self.execute, maxInterval)
          return
        }

        // Otherwise, reschedule at the remaining amount of time and set remaining to 0
        self.token = setTimeout(self.execute, self.remainingMs)
        self.remainingMs = 0
      }

      self.setArgs = self.setArgs.bind(self)
      self.start = self.start.bind(self)
      self.stop = self.stop.bind(self)
    }
    setArgs (args) {
      let self = this

      if (typeof args !== 'object') {
        throw new Error('args must be an object')
      }

      Object.keys(args).forEach(arg => {
        self.args[arg] = args[arg]
      })
    }
    start () {
      if (this.interval > maxInterval) {
        this.remainingMs = this.interval - maxInterval
        this.token = setTimeout(this.execute, maxInterval)
      } else {
        this.token = setInterval(this.execute, this.interval)
      }
    }
    stop () {
      if (this.interval > maxInterval) {
        clearTimeout(this.token)
      } else {
        clearInterval(this.token)
      }
    }
  }

  return SuperInterval
}

module.exports = getSuperInterval
