const Superinterval = require('../src/superinterval')(1000)

exports.setArgs = test => {
  let executionCount = 0
  let handler = args => {
    if (executionCount > 0) {
      test.deepEqual(args, { arg1: 10 })
    } else {
      test.deepEqual(args, { arg1: 4 })
    }
    executionCount++
  }
  let interval = new Superinterval(handler, 2000, { arg1: 4 })

  test.expect(2)

  setTimeout(() => interval.setArgs({ arg1: 10 }), 3000)
  setTimeout(() => {
    interval.stop()
    test.done()
  }, 5000)

  interval.start()
}
